// never end a pass on equal i and j!!!
// determine the split from the way i and j "topple"

import java.util.Arrays;

public class Sort {

 public static void main(String[] args) {
  //int[] test = new int[] {6,6,5,3,1,8,7,2,4};
  int[] test = new int[] {1,2,1};
  System.out.println(Arrays.toString(test));
  qpass(test, 0, test.length);
  System.out.println(Arrays.toString(test));
 }

 // let the pivot be part of the lower group

 public static void qpass(int[] target, int from, int to) {
  if (from + 1 == to) return;
  int pivot = three_median(
   target[from],
   target[to - 1],
   target[to / 2]
  );
  int i = from;
  int j = to - 1;
  while (i <= j) {
   if (target[i] >= pivot && target[j] <= pivot) swap(target, i, j);
   if (target[i] <= pivot) i++;
   if (target[j] >= pivot) j--;
  }
  qpass(target, i, to);
  qpass(target, from, j + 1);
 }

 public static void swap(int[] target, int i, int j) {
  int acc = target[i];
  target[i] = target[j];
  target[j] = acc;
 }

 public static int three_median(int i, int j, int k) {
  int min = Math.min(i, Math.min(j, k));
  int max = Math.max(i, Math.max(j, k));
  if (i >= min && i <= max) return i;
  else if (j >= min && j <= max) return j;
  else return k;
 }
}
